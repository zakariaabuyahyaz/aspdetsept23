﻿using fourpm.Models;
using Microsoft.Data.SqlClient;

namespace fourpm.Repository
{
    public class CategoryRepository
    {
        SqlConnection con;
        SqlCommand cmd;

        public CategoryRepository()
        {
            con = new SqlConnection("server=localhost; database=shiftthree; user id=sa; password=per457@.Q; TrustServerCertificate=True");
        }

        public List<Category> getAll()
        {
            List<Category> data = new List<Category>();
            using (con)
            {
                con.Open();
                string _query = "select * from category order by id desc";
                cmd = new SqlCommand(_query, con);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int _id = Convert.ToInt32(dr["id"]);
                    string _name = Convert.ToString(dr["name"]);

                    data.Add(new Category() { id = _id, name = _name });
                }
            }
            return data;
        }

        public Category get_by_id(int id)
        {
            Category data = new Category();
            using (con)
            {
                con.Open();
                string _query = $"select * from category where id={id}";
                cmd = new SqlCommand(_query, con);
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                int _id = Convert.ToInt32(dr["id"]);
                string _name = Convert.ToString(dr["name"]);

                data = new Category() { id = _id, name = _name };

            }
            return data;
        }

        public bool create(string name)
        {
            using(con)
            {
                con.Open();
                string _stmt = $"insert into category values('{name}')";
                cmd = new SqlCommand(_stmt, con);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool update(int id, string newname)
        {
            using (con)
            {
                con.Open();
                string _stmt = $"update category set name='{newname}' where id={id}";
                cmd = new SqlCommand(_stmt, con);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool delete(int id)
        {
            using (con)
            {
                con.Open();
                string _stmt = $"delete from category where id={id}";
                cmd = new SqlCommand(_stmt, con);
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }



    }
}
