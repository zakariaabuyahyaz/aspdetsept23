﻿using fourpm.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace fourpm.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult layoutexample()
        {
            return View();
        }

        public IActionResult layoutexample2()
        {
            return View();
        }

        public IActionResult details()
        {
            string magaca = Convert.ToString(Request.Query["magaca"]);
            int percent = Convert.ToInt32(Request.Query["tirada"]);

            ViewData["magaca"] = magaca;
            ViewData["tirada"] = percent;

            if(percent>50)
            {
                ViewData["color"] = "green";
                ViewData["msg"] = "Conguratualtion, You Win in the Election";
            }else if(percent>25 && percent < 50)
            {
                ViewData["color"] = "yellow";
                ViewData["msg"] = "Continue, You are in the middle";
            }
            else
            {
                ViewData["color"] = "pink";
                ViewData["msg"] = "You Fail !!";
            }
            return View();

        }

        public IActionResult login()
        {
            string username = Request.Query["user"].ToString();
            HttpContext.Session.SetString("username", username);
            return RedirectToAction("displaysession");

        }

        public IActionResult displaysession()
        {
            string x = HttpContext.Session.GetString("username");
            return Content($"your username is: {x}");

        }

        public IActionResult logout()
        {
            HttpContext.Session.Remove("username");
            return RedirectToAction("displaysession");

        }
    }
}