﻿using fourpm.Models;
using Microsoft.AspNetCore.Mvc;

namespace fourpm.Controllers
{
    public class CalculatorController : Controller
    {
        public IActionResult New()
        {
            ViewData["is_hidden"] = true;
            return View();
        }

        [HttpPost]
        public IActionResult New(MyData modal)
        {
            double total = 0;
            switch(modal.opr)
            {
                case '+':
                    total = modal.numberone+modal.numbertwo;
                    break;
                case '-':
                    total = modal.numberone - modal.numbertwo;
                    break;
                case '*':
                    total = modal.numberone * modal.numbertwo;
                    break;
                default:
                    total = 0;
                    break;
            }
            ViewData["total"]=total;
            ViewData["is_hidden"] = false; ;
            return View();
        }
    }
}
