﻿using Microsoft.AspNetCore.Mvc;

namespace fourpm.Controllers
{
    public class BankController : Controller
    {
        public IActionResult transfers()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult sendmoney()
        {
            string receiver = Request.Form["receiver"];
            string amount = Request.Form["amount"];

            // send to db
            //send email notification

            return Content($"Success: money sent to {receiver}");

        }
    }
}
