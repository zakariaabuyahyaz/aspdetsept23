﻿using fourpm.Data;
using fourpm.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;

namespace fourpm.Controllers
{
    public class TeacherController : Controller
    {
        private readonly dbContext _context;
        public TeacherController(dbContext context)
        {
            _context = context;
        }
    
        public IActionResult Index() //action method
        {
            List<Teacher> modelData = _context.Teachers
                .OrderByDescending(x => x.Id)
                .ToList();
            return View(modelData); 


        }
        
        public IActionResult Create()  // httpget
        {
            Teacher modal=  new Teacher();  
            return View(modal); 
        }

        [HttpPost]  //action verbs
        public IActionResult Create(Teacher model)
        {
            _context.Teachers.Add(model);  
            _context.SaveChanges();
            return RedirectToAction("Index");   

        }

        public IActionResult Edit(int id)  //cont/action/id
        {
            var found = _context.Teachers.Find(id);
            return View(found);
        }

        [HttpPost]
        public IActionResult Edit(Teacher model)
        {
            var found = _context.Teachers.Find(model.Id);
            found.Name=model.Name;
            found.Email=model.Email;    
            found.Mobile=model.Mobile;  
            _context.Teachers.Update(found);    
            _context.SaveChanges(); 
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var found = _context.Teachers.Find(id);
            return View(found);
        }

        [HttpPost]
        public IActionResult Delete(Teacher model)
        {
            var found = _context.Teachers.Find(model.Id);

            _context.Teachers.Remove(found);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
