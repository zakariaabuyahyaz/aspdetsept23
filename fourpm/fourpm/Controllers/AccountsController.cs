﻿using fourpm.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Data.SqlClient;
using System.Security.Claims;

namespace fourpm.Controllers
{
    public class AccountsController : Controller
    {
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(loginmodel model)
        {
            string _connectionstring = "server=localhost; database=shiftthree; user id=sa; password=per457@.Q; TrustServerCertificate=true;";
            //string _connectionstring = "server=localhost; database=shiftthree; integrated secutiry=true; TrustedServerCertificate=true;";
            using(SqlConnection con=new SqlConnection(_connectionstring))
            {
                con.Open();
                string query = $"select count(username) result from users where username='{model.username}' and password='{model.password}'";
                SqlCommand cmd = new SqlCommand(query, con);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                if (count > 0)
                {
                    //give user a session
                    HttpContext.Session.SetString("username", model.username);

                    //give auth cookie
                    var claims = new List<Claim>() { 
                        new Claim(ClaimTypes.NameIdentifier,model.username),
                        new Claim(ClaimTypes.Role, "Finance")
                    
                    };

                    var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    var princible = new ClaimsPrincipal(identity);
                    HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, princible);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    //invalid credentials
                    ViewData["error"] = "Invalid Credentials";
                    return View(model);

                }

            }
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Remove("username");
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}
