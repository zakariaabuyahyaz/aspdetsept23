﻿using fourpm.Models;
using Microsoft.AspNetCore.Mvc;

namespace fourpm.Controllers
{
    public class MacmiilController : Controller
    {
        public IActionResult Index()  // action
        {
            //here i am creating the list
            List<customer> list= new List<customer>();
            list.Add(new customer() { id=100, name="Ali", mobile="25263256666"});
            list.Add(new customer() { id = 102, name = "Omer", mobile = "25263251116" });
            //list.Add(new customer() { id = 104, name = "Amina", mobile = "252632424266" });

            //using viewdata to send data to view
            ViewData["title"] = "List of Customers";
            ViewBag.count = list.Count;  //2
            return View(list);  //sending model
        }

        public IActionResult New()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Data(customer x)
        {
            //send to database
            return View(x);
        }
    }
}
