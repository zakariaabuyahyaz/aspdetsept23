﻿namespace fourpm.Models
{
    public class remittence
    {
        public string sender { get; set; }
        public string receiver { get; set; }
        public double amount { get; set; }
        public string remarks { get; set; }
    }
}
