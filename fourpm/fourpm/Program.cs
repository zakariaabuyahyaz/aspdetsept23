using fourpm.Data;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;

namespace fourpm
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllersWithViews();
            var _con = builder.Configuration.GetConnectionString("mycon");
            builder.Services.AddDbContext<dbContext>(p => p.UseSqlServer(_con));
            builder.Services.AddDistributedMemoryCache();
            builder.Services.AddSession(options => 
            {
                options.IdleTimeout = TimeSpan.FromMinutes(10);
                options.Cookie.Name = "fourpm";
                options.Cookie.HttpOnly= true; //secutiry
                options.Cookie.Path = "/";
                options.Cookie.IsEssential= true;
                options.Cookie.SecurePolicy = CookieSecurePolicy.None;
            });

            builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options => 
            {
                options.LoginPath = "/Accounts/Login";
                options.ExpireTimeSpan= TimeSpan.FromMinutes(2);
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();


            //your middle ware

            app.UseRouting();  // routing middle ware
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseSession();

            app.MapControllerRoute(
                name:"blognews",
                pattern:"news/{id}",
                defaults: new { controller="News", action="findnews"}


                );

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            app.Run();
        }
    }
}