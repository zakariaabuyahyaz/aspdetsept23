﻿namespace shift3.Models
{
    public class resources
    {
        public int id { get; set; }
        public int catid { get; set; }
        public string name { get; set; }
        public string ip { get; set; }
        public string remarks { get; set; }
        public string manufacturer { get; set; }
        public string createdby { get; set; }
        public DateTime createdate { get; set; }
    }
}
