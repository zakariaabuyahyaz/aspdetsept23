﻿namespace shift3.Models
{
    public class VoteResult
    {
        public string party { get; set; }
    }

    public class VoteResults
    {
        public string party { get; set; }
        public double perc { get; set; }
    }
}
