﻿using System;
using System.Collections.Generic;

namespace shift3.Models;

public partial class Teacher
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string? Email { get; set; }

    public string? Mobile { get; set; }
}
