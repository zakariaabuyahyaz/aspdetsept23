﻿namespace shift3.Models
{
    public class remittence
    {
        public string receiver { get; set; }
        public string sender { get; set; }
        public double amount { get; set; }
        public string remarks { get; set; }
    }
}
