using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using shift3.Data;

namespace shift3
{
    public class Program
    {
        public static void Main(string[] args)  
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllersWithViews();
            var _con = builder.Configuration.GetConnectionString("mydb");
            builder.Services.AddDbContext<dbContext>(p => p.UseSqlServer(_con));
            builder.Services.AddDistributedMemoryCache();
            builder.Services.AddSession(options => 
            {
                options.IdleTimeout = TimeSpan.FromMinutes(2);
                options.Cookie.Name = "shiftthree_Cookie";
                options.Cookie.HttpOnly = false;
                options.Cookie.Path= "/";
                options.Cookie.SecurePolicy= CookieSecurePolicy.None;
            });

            builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
            {
                options.LoginPath = "/accounts/login";
                options.ExpireTimeSpan = TimeSpan.FromMinutes(3);
            });

            var app = builder.Build();



            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            

            app.UseRouting();
            app.UseAuthentication();//middle ware
            app.UseAuthorization();
            app.UseSession(); //middleware
            app.MapControllerRoute(
                name:"newsroute",
                pattern:"wararka/{id?}",
                defaults:new { controller="News", action="find"});

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            app.Run();
        }
    }
}