﻿using Microsoft.AspNetCore.Mvc;
using shift3.Models;

namespace shift3.Views.Shared.Components
{
    public class publicvotesViewComponent:ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            //connect to database
            //if (1 >= 2)
            //{
            //    //send email 
            //    // do this
            //}

            //for(var i=1; i<10; i++)
            //{
            //    //do some thing
            //}

            List<VoteResults> votes = new List<VoteResults>();
            votes.Add(new VoteResults() { party = "Kulmiye", perc=40 });
            votes.Add(new VoteResults() { party = "Wadani", perc=40 });
            votes.Add(new VoteResults() { party = "Ucid", perc=40 });

            return View("Default",votes);
        }
    }
}
