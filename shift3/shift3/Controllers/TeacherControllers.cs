﻿using Microsoft.AspNetCore.Mvc;
using shift3.Data;
using shift3.Models;

namespace shift3.Controllers
{
    public class TeacherController : Controller
    {
        private readonly dbContext _dbContext;

        public TeacherController(dbContext dbContext)
        {
            _dbContext = dbContext; 
        }

        public IActionResult Index()
        {
            var modaldata = _dbContext.Teachers
                .OrderByDescending(x => x.Id)   
                .ToList();

            return View(modaldata);
        }

        public IActionResult Create()   //httpget
        {
            var newmodal = new Teacher();
            return View(newmodal);
        }

        [HttpPost]
        public IActionResult Create(Teacher newmodal)
        {
            _dbContext.Teachers.Add(newmodal);  
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var foundTeacher = _dbContext.Teachers.Find(id);
            return View(foundTeacher);
        }

        [HttpPost]
        public IActionResult Edit(Teacher newmodal)
        {
            var found = _dbContext.Teachers.Find(newmodal.Id);
            found.Name = newmodal.Name;
            found.Email = newmodal.Email;   
            found.Mobile= newmodal.Mobile;  
            _dbContext.Teachers.Update(found);              
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var foundTeacher = _dbContext.Teachers.Find(id);
            return View(foundTeacher);
        }

        [HttpPost]
        public IActionResult Delete(Teacher newmodal)
        {
            var found = _dbContext.Teachers.Find(newmodal.Id);
            _dbContext.Teachers.Remove(found);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}
