﻿using Microsoft.AspNetCore.Mvc;
using shift3.Models;
using shift3.Repository;

namespace shift3.Controllers
{
    public class CategoryController : Controller
    {
        public IActionResult Index()
        {
            var repo = new CategoryRepo();
            var data = repo.getAll();
            return View(data);
        }

        public IActionResult Create()
        {
            return View();

        }

        [HttpPost]
        public IActionResult Create(category model)
        {
            var repo = new CategoryRepo();
            repo.create(model.name);
            return RedirectToAction("Index");

        }

        public IActionResult Edit(int id)
        {
            var repo = new CategoryRepo();  
            category found= repo.get_by_id(id); 
            return View(found);

        }

        [HttpPost]
        public IActionResult Edit(category model)
        {
            var repo = new CategoryRepo();
            repo.update(model.id, model.name);
            return RedirectToAction("Index");   
        }

        public IActionResult Delete(int id)
        {
            var repo = new CategoryRepo();
            category found = repo.get_by_id(id);
            return View(found);

        }

        [HttpPost]
        public IActionResult Delete(category model)
        {
            var repo = new CategoryRepo();
            repo.delete(model.id);
            return RedirectToAction("Index");
        }
    }
}
