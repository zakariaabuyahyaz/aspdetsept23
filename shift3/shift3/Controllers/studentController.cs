﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace shift3.Controllers
{
    [Authorize]
    public class studentController : Controller
    {

        //i am creating method/action
        //always actions are located inside controller
        [Route("api/dahabshiil/contacts/connect")]   //attribute routing
        public string salaan()
        {
            return "ASC, Iska waran";
        }

        [Route("api/somtel/contacts/connect")]
        public string getUniversity()
        {
            return "My university is: Timaade";
        }

        public string news(int id)
        {
            string newsdata = "";
            if(id==10)
            {
                newsdata = "There is Jihad between Israel and Palestine";
            }else if (id == 1000)
            {
                newsdata = "S/Land has opened new office buildings in the down town";
            }
            else
            {
                newsdata = "Invalid news id";
            }

            return newsdata;
        }
       
    }
}
