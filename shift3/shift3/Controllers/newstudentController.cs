﻿using Microsoft.AspNetCore.Mvc;
using shift3.Models;

namespace shift3.Controllers
{
    public class newstudentController : Controller
    {
        public IActionResult getStudent(){

            student objname=new student();
            objname.id = 120;
            objname.name = "Omer Ali";
            objname.email = "omer@gmail.com";
            return View(objname); 
        }

        [NonAction]   //action selectors
        [ActionName("sooraadi")]
        [HttpGet]   //

        public  IActionResult findall()  //action
        {
            List<student> modalData = new List<student>();
            modalData.Add(new student() { id=1, name="Ahmed Geedi", email="Email@gmail.com", mobile="252634415566" });
            modalData.Add(new student() { id = 2, name = "Amina Geedi", email = "Email@gmail.com", mobile = "252634415566" });
            modalData.Add(new student() { id = 3, name = "Casha Geedi", email = "Email@gmail.com", mobile = "252634415566" });
            modalData.Add(new student() { id = 4, name = "Ibrahim Geedi", email = "Email@gmail.com", mobile = "252634415566" });

            return View(modalData);
        }
    }
}
