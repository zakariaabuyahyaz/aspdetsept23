﻿using Microsoft.AspNetCore.Mvc;

namespace shift3.Controllers
{
    public class examsController : Controller
    {
        public string getdeadline()
        {
            return "Exams will start on 18th Oct, 2023";
        }

        public string getmarks(int id)
        {
            string marks = "";
            if (id == 15)
            {
                marks = "25";
            }else if (id == 65)
            {
                marks = "99";
            }
            else
            {
                marks = "0";
            }
            return marks;   
        }
    }
}
