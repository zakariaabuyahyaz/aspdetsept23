﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using shift3.Models;
using System.Diagnostics;


namespace shift3.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            if(HttpContext.Session.GetString("username")==null)
            {
                return RedirectToAction("Login", "Accounts");
            }

            ViewData["currentuser"] = HttpContext.Session.GetString("username");
            return View();
        }

        [AllowAnonymous]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult layoutExample()
        {
            return View();
        }

        public IActionResult layoutExample2()
        {
            return View();
        }

        public IActionResult details()
        {
            string party = Convert.ToString(Request.Query["party"]);
            double perc = Convert.ToDouble(Request.Query["percent"]);
            ViewData["party"] = party;
            ViewData["percent"] =perc;
            return View();
        }

        public IActionResult storeinsession()
        {
            string v = Request.Query["user"].ToString();
            HttpContext.Session.SetString("uname",v);
            return RedirectToAction("getsession");

        }

        public IActionResult getsession()
        {
            string uname = HttpContext.Session.GetString("uname");
            return Content(uname);
        }


    }
}