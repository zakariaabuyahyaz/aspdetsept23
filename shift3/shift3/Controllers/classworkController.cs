﻿using Microsoft.AspNetCore.Mvc;
using shift3.Models;

namespace shift3.Controllers
{
    public class classworkController : Controller
    {
        public IActionResult details(int id)
        {
            products modal= new products(); 
            if(id==1)
            {
                modal.id = 1;
                modal.name = "Computer HP";
                modal.category = "Electronics";
                modal.price = 20.5m;
            }
            else
            {
                modal.id = id;
                modal.name = "Mouse";
                modal.category = "Electronics";
                modal.price = 1.25m;
            }
            return View(modal);
        }
    }
}
