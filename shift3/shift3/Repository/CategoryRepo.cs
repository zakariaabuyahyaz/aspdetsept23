﻿using Microsoft.Data.SqlClient;
using shift3.Models;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace shift3.Repository
{
    public class CategoryRepo
    {
        SqlConnection con;
        SqlCommand cmd;

        public CategoryRepo()
        {
            con= new SqlConnection("server=localhost; database=shiftthree; user id=sa; password=per457@.Q; TrustServerCertificate=true;");   
        }

        public List<category> getAll()
        {
            List<category> list = new List<category>();
            using (con)
            {
                con.Open();
                string _query = $"select * from category order by name asc";
                cmd = new SqlCommand(_query, con);
                
                SqlDataReader dr=cmd.ExecuteReader();
                while (dr.Read()) 
                {
                    list.Add(new category() { id = Convert.ToInt32(dr["id"]), name = dr["name"].ToString() });
                }
            }
            return list;
        }

        public category get_by_id(int id)
        {
            category data= new category();
            using (con)
            {
                con.Open();
                string _query = $"select * from category where id={id}";
                cmd = new SqlCommand(_query, con);

                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    data = new category() { id = Convert.ToInt32(dr["id"]), name = dr["name"].ToString() };
                }
            }
            return data;
        }

        public bool create(string name)
        {
            using (con)
            {
                con.Open();
                string _query = $"insert into category values('{name}')";
                cmd = new SqlCommand(_query, con);

                int count = cmd.ExecuteNonQuery();
                if(count>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool update(int id, string newname)
        {
            using (con)
            {
                con.Open();
                string _query = $"update category set name='{newname}' where id={id}";
                cmd = new SqlCommand(_query, con);

                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool delete(int id)
        {
            using (con)
            {
                con.Open();
                string _query = $"delete from category where id={id}";
                cmd = new SqlCommand(_query, con);

                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    }
}
